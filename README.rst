==================================
Terraform to provision GKE cluster
==================================

.. code-block:: bash
    $ cd terraform/
    $ terraform init
    $ terraform plan -out=tfplan
    $ terraform apply "tfplan"

====================
Kubernetes manifests
====================

* Deployment
* Service
* GKE Ingress
* Default backend for Ingress

Ref: https://github.com/kubernetes/kubernetes/issues/20555

* SSL cert (have not implemented yet)

============================
Gitlab Continuous deployment
============================

Have not implemented yet
